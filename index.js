#!/usr/bin/env node

const fs = require('fs')
const path = require('path')

const DecompressZip = require('decompress-zip')

function findPackagePath (packageName) {
  // make symlinks happy
  const containingNodeModulesPath = path.resolve(process.argv[1], '..', '..')
  const searchPaths = require.resolve.paths(packageName).concat(containingNodeModulesPath)
  const parentPath = searchPaths.find(lookupPath => fs.existsSync(path.resolve(lookupPath, packageName)))

  if (!parentPath) {
    throw new Error(`Could not find package ${packageName} in ${searchPaths.join(', ')} !`)
  }

  return path.resolve(parentPath, packageName)
}

const cliArguments = process.argv.splice(2)
if (cliArguments.length !== 1) {
  const packageJson = require('./package.json')
  console.log(`Usage: ${Object.keys(packageJson.bin)[0]} OUTPUT_PATH\n`)
  process.exit(1)
}

const [outputPath] = cliArguments

if (!fs.existsSync(outputPath)) {
  console.log(`Creating directory ${outputPath} ...`)
  fs.mkdirSync(outputPath)
}

const inputPath = path.resolve(findPackagePath('kotlin-compiler'), 'lib', 'kotlin-stdlib-js.jar')
console.log(`Extracting ${inputPath} to ${path.resolve(outputPath)} ...`)

const unzipper = new DecompressZip(inputPath)

unzipper.on('error', (error) => {
  console.error(error.message)
  process.exit(1)
})

unzipper.on('extract', (result) => {
  const fileNames = result.map(item => item.deflated)
  console.log(`Extracted the following files: ${fileNames.join(', ')}`)
})

unzipper.extract({
  path: outputPath,
  filter (file) {
    return file.filename.indexOf('kotlin.js') === 0
  }
})
